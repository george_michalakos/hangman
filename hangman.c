#include <stdio.h>
#include <stdlib.h>

void draw(const char guesses, const char *array, const size_t arraylen, const char *misses, const size_t misseslen)
{
    // Print the correct hangman aschii art based on the number of guesses.
    switch(guesses)
    {
        case 6:
            printf("\n\t |---\n\t    |\n\t    |\n\t    |\n\t____|\n\n\t");
        break;
        case 5:
            printf("\n\t |---\n\t o  |\n\t    |\n\t    |\n\t____|\n\n\t");
        break;
        case 4:
            printf("\n\t |---\n\t o  |\n\t |  |\n\t    |\n\t____|\n\n\t");
        break;
        case 3:
            printf("\n\t |---\n\t o  |\n\t/|  |\n\t    |\n\t____|\n\n\t");
        break;
        case 2:
            printf("\n\t |---\n\t o  |\n\t/|\\ |\n\t    |\n\t____|\n\n\t");
        break;
        case 1:
            printf("\n\t |---\n\t o  |\n\t/|\\ |\n\t/   |\n\t____|\n\n\t");
        break;
        case 0:
            printf("\n\t |---\n\t o  |\n\t/|\\ |\n\t/ \\ |\n\t____|\n\n\t");
        break;
    }

    // Print the word array.
    for(size_t i = 0; i < arraylen; i++)
        printf("%c",array[i]);
    
    printf(" : ");
    
    // Print the missed characters.
    for(size_t i = 0; i < misseslen; i++)
        printf("%c ",misses[i]);
}


int main(int argc, char **argv)
{
    // Get input from the command line regarding the word.
    char *word = "hangman", *chars = "";
    if(argc > 1) word = argv[1];
    if(argc > 2) chars = argv[2];
    size_t wordlen = 0;
    while(word[wordlen]) wordlen++;

    // Initialize the word array and reveal characters.
    char array[wordlen], found = 0;
    for(size_t i = 0; word[i]; i++)
    {
        array[i] = '_';
        for(size_t j = 0; chars[j]; j++)
            if(word[i] == chars[j]) array[i] = word[i];
    }   

    // The main game loop.
    // It stops when the player loses or their lives or when the player guesses all the letters correctly.
    char misses[256], guesses = 6, complete = 0;
    size_t misseslen = 0;
    while(guesses && !complete)
    {
        // Clear the screen and draw the game.
        // Prompt the user to enter a character that is not a space or a new line.
        char guess;
        do 
        {
            system("clear");
            draw(guesses,array,wordlen,misses,misseslen);

            printf("\n\t>> ",array,misses);
            guess = fgetc(stdin);
        }
        while(guess == '\n');

        // Skip all the characters on stdin until we find a new line.
        while(fgetc(stdin) != '\n');

        // If the letter is exists within the given word.
        // Add it to the word array.        
        complete = 1;
        found = 0;
        for(size_t i = 0; i < wordlen; i++)
        {
            if(word[i] == guess)
            {
                array[i] = guess;
                found = 1;
            }
            
            if(word[i] != array[i]) complete = 0;
        }
        
        // If the given word does not contain the letter.
        if(!found)
        {
            // Check to see if the letter exists inside the missed characters array.
            for(size_t i = 0; i < misseslen; i++)
                if(misses[i] == guess) found = 1;
            
            // If the letter doesn't exist within the missed characters array and the array buffer is not full.
            // Decrese the number of guesses and add the character to the missed characters array.
            if(misseslen >= 256) return 1;
            else if(!found)
            {
                misses[misseslen++] = guess;
                guesses--;
            }
        }
    }

    // Clear the screen and draw the game.
    system("clear");
    draw(guesses,array,wordlen,misses,misseslen);

    // Check to see if the user won or lost the game.
    if(complete) printf("\n\tYou guessed all the letters!\n\n");
    else printf("\n\tThe word was %s.\n\n",word);
    return 0;
}

# Hangman
A simple terminal [hangman](https://en.wikipedia.org/wiki/Hangman_(game)) game, written in c.

![hangman.png](/hangman.png)

### Build Instructions
First **clone the repository**: `user@hostname:~$ git clone https://gitlab.com/george_michalakos/hangman.git`

Next execute the **build command** (You can replace the ' ~/bin/ ' with whatever directory you want): `user@hostname:~$ gcc --std=c99 hangman/hangman.c -o ~/bin/hangman`

Optionaly **remove the folder** with: `user@hostname:~$ rm -rf hangman`

### Hangman Syntax
Hangman takes two arguments. The word or sentence you want to guess and the characters you want to be revealed at the start of the game.

ex: `user@hostname:~$ hangman 'hello world!!' ' !'` `user@hostname:~$ hangman 'parrot' 'pt'`

### Play Hangman With A Random Word Form A File

If you want to use a different file, replace 'words.txt' with the path to another file and enter the command shown below to your terminal. The syntax of this file has to be the same with the provided words.txt file.

ex: `user@hostname:~$ hangman "$(sed -n $(expr $RANDOM % $(cat words.txt | wc -l))p words.txt)"`
